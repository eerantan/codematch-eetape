<?php

namespace Survey\SurveyPage\Api\Data;

interface AnswerInterface
{
    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const ANSWER_TABLE = 'survey_answer';
    const ANSWER_ID = 'answer_id';
    const USER_FRIENDLY = 'user_friendly';
    const EASY_TO_FIND = 'easy_to_find';
    const USE_LAPTOP = 'use_laptop';
    const USE_MOBILE = 'use_mobile';
    const USE_MOUSE = 'use_mouse';
    const USE_KEYBOARD = 'use_keyboard';
    const OWN_COMMENTS = 'own_comments';
    const GENDER = 'gender';
    const AGE_GROUP = 'age_group';
    const CREATED_TIMESTAMP = 'created_timestamp';
    /**#@-*/

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();
    
    /**
     * Get Created Timestamp
     *
     * @return string|null
     */
    public function getCreatedTimeStamp();
    
    /**
     * Get User Friendly
     *
     * @return int|null
     */
    public function getUserFriendly();

    /**
     * Get Easy to find
     *
     * @return int|null
     */
    public function getEasyToFind();

    /**
     * Get Use Laptop
     *
     * @return int|null
     */
    public function getUseLaptop();

    /**
     * Get Use Mobile
     *
     * @return int|null
     */
    public function getUseMobile();

    /**
     * Get Use Mouse
     *
     * @return int|null
     */
    public function getUseMouse();

    /**
     * Get Use Keyboard
     *
     * @return int|null
     */
    public function getUseKeyboard();

    /**
     * Get Own Comments
     *
     * @return string|null
     */
    public function getOwnComments();

    /**
     * Get Gender
     *
     * @return int|null
     */
    public function getGender();

    
    /**
     * GetAge Group
     *
     * @return int|null
     */
    public function getAgeGroup();

    /**
     * Set ID
     *
     * @param int $id
     * @return $this
     */
    public function setId($id);
    
    /**
     * Set Created Timestamp
     *
     * @param string $createdAt
     * @return $this
     */
    public function setCreatedTimeStamp($createdAt);
    
    /**
     * Set User Friendly
     *
     * @param int $userFriendly
     * @return $this
     */
    public function setUserFriendly($userFriendly);

    /**
     * Set Easy to find
     *
     * @param int $easyToFind
     * @return $this
     */
    public function setEasyToFind($easyToFind);

    /**
     * Set Use Laptop
     *
     * @param int $useLaptop
     * @return $this
     */
    public function setUseLaptop($useLaptop);

    /**
     * Set Use Mobile
     *
     * @param int $useMobile
     * @return $this
     */
    public function setUseMobile($useMobile);

    /**
     * Set Use Mouse
     *
     * @param int $useMouse
     * @return $this
     */
    public function setUseMouse($useMouse);

    /**
     * Set Use Keyboard
     *
     * @param int $useKeyboard
     * @return $this
     */
    public function setUseKeyboard($useKeyboard);

    /**
     * Set Own Comments
     *
     * @param string $ownComments
     * @return $this
     */
    public function setOwnComments($ownComments);

    /**
     * Set Gender
     *
     * @param int $gender
     * @return $this
     */
    public function setGender($gender);

    
    /**
     * Set Age Group
     *
     * @param int $ageGroup
     * @return $this
     */
    public function setAgeGroup($ageGroup);    

}

