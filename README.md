
Magento exercise in groups of 3

Implement your own customer survey module to Magento. You can decide the questions yourself. The module should, however, have the following features:

1. A custom HTML form to which the survey answers are given
2. Implement at least one question which has products from the store as survey options. Read the products by using Magento's own product repository.
3. A database table for the survey answers created by the module.
4. A server-side controller that validates the user answer data and stores answers to the table.
5. A redirect page which reports to the user if the answer data was received succesfully or not.
6. Create a result widget which shows textual statistics of the survey answers. The widget should have some widget options.

Extra: A front-end Javascript component that renders, for example, a result graph based on the survey answers.

Check out issues for further instructions.

