<?php

namespace Survey\SurveyPage\Controller\Index;

use Magento\Framework\App\Action\Context;

class Results extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
    protected $answerFactory;
 
    public function __construct(
            Context $context,
            \Magento\Framework\View\Result\PageFactory $resultPageFactory,
            \Survey\SurveyPage\Model\AnswerFactory $answerFactory
            )
    {
        $this->_resultPageFactory = $resultPageFactory;
        $this->answerFactory = $answerFactory;
        parent::__construct($context);
    }
 
    public function execute()
    {   
        $data = $this->getRequest()->getPost();
        
        $model = $this->answerFactory->create();

        $model->setUserFriendly($data['question1']);
        $model->setEasyToFind($data['question2']);
        $model->setUseLaptop($data['question3a']);
        $model->setUseMobile($data['question3b']);
        $model->setUseMouse($data['question3c']);
        $model->setUseKeyboard($data['question3d']);
        $model->setOwnComments($data['comment']);
        $model->setGender($data['gender']);
        $model->setAgeGroup($data['age']);

        $msg ="";

        if($data != ''){ 
            $model->save();
            // $this->getresult->getdirected
            $msg = 'Answer saved, thank you for your answers!';
        }else{
             $msg = 'not saved';  
        } 
        echo $msg;
        
    }
}