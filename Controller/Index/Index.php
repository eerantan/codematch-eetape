<?php

namespace Survey\SurveyPage\Controller\Index;

use Magento\Framework\App\Action\Context;
use Survey\SurveyPage\Model\Answer;
 
class Index extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
    protected $answerFactory;
 
    public function __construct(
            Context $context,
            \Magento\Framework\View\Result\PageFactory $resultPageFactory,
            \Survey\SurveyPage\Model\AnswerFactory $answerFactory
            )
    {
        $this->_resultPageFactory = $resultPageFactory;
        $this->answerFactory = $answerFactory;
        parent::__construct($context);
    }
 
    public function execute()
    {
        $resultPage = $this->_resultPageFactory->create();
        return $resultPage;
        
    }
}