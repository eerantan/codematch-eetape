<?php

namespace Survey\SurveyPage\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class Recurring implements InstallSchemaInterface
{
    const ANSWER_TABLE = 'survey_answer';
    const ANSWER_ID = 'answer_id';
    const USER_FRIENDLY = 'user_friendly';
    const EASY_TO_FIND = 'easy_to_find';
    const USE_LAPTOP = 'use_laptop';
    const USE_MOBILE = 'use_mobile';
    const USE_MOUSE = 'use_mouse';
    const USE_KEYBOARD = 'use_keyboard';
    const OWN_COMMENTS = 'own_comments';
    const GENDER = 'gender';
    const AGE_GROUP = 'age_group';
    const CREATED_TIMESTAMP = 'created_timestamp';
    
    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    private $coreResource;

    /**
     * @param \Magento\Framework\App\ResourceConnection $coreResource
     */
    public function __construct(
        \Magento\Framework\App\ResourceConnection $coreResource
    ) {
        $this->coreResource = $coreResource;
    }

    /**
     * Stub for recurring setup script.
     *
     * For quick development and prototyping purposes we re-create our tables during every call to setup:upgrade,
     * regardless of version upgrades, which you normally would not do in production.
     *
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $setup
     * @throws
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $connection = $this->coreResource->getConnection();

        // Drop existing tables for quick prototyping. Comment out if you don't want this!
        if ($connection->isTableExists(self::ANSWER_TABLE)) {
            $connection->dropTable(self::ANSWER_TABLE);
        }

        $surveyTable = $connection->newTable(
            self::ANSWER_TABLE
        )   
        ->addColumn(
            self::ANSWER_ID,
            Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Answer ID'
        )
        ->addColumn(
            self::CREATED_TIMESTAMP,
            Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
            'Created Timestamp'      
        )
        ->addColumn(
            self::USER_FRIENDLY,
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false],
            'User Friendly'
        )
        ->addColumn(
            self::EASY_TO_FIND,
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false],
            'Easy to Find'
        )
        ->addColumn(
            self::USE_LAPTOP,
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false],
            'Use Laptop'
        )
        ->addColumn(
            self::USE_MOBILE,
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false],
            'Use Mobilephone'
        )   
        ->addColumn(
            self::USE_MOUSE,
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false],
            'Use Mouse'
        )       
        ->addColumn(
            self::USE_KEYBOARD,
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false],
            'Use Keyboard'
        )          
        ->addColumn(
            self::OWN_COMMENTS,
            Table::TYPE_TEXT,
            null,
            ['nullable' => false],
            'Own Comments'      
        )      
        ->addColumn(
            self::EASY_TO_FIND,
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false],
            'Easy to Find'      
        )
        ->addColumn(
            self::GENDER,
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false],
            'Gender'      
        )
        ->addColumn(
            self::AGE_GROUP,
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false],
            'Age Group'      
        );
        
        $connection->createTable($surveyTable);

        $setup->endSetup();
    }
}