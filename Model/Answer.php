<?php

namespace Survey\SurveyPage\Model;

use \Magento\Framework\Model\AbstractModel;
use \Magento\Framework\DataObject\IdentityInterface;
use \Survey\SurveyPage\Api\Data\AnswerInterface;

/**
 * Class File
 * @package Survey\SurveyPage\Model
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Answer extends AbstractModel implements AnswerInterface, IdentityInterface
{
    /**
     * Cache tag
     */
    const CACHE_TAG = 'survey_answer';

    /**
     * Answer Initialization
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Survey\SurveyPage\Model\ResourceModel\Answer::class);
    }

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId()
    {
        return $this->getData(self::ANSWER_ID);
    }    
    /**
     * Get Created Timestamp
     *
     * @return string|null
     */
    public function getCreatedTimeStamp()
    {
        return $this->getData(self::CREATED_TIMESTAMP);
    }
    /**
     * Get User Friendly
     *
     * @return int|null
     */
    public function getUserFriendly()
    {
        return $this->getData(self::USER_FRIENDLY);
    }

    /**
     * Get Easy to find
     *
     * @return int|null
     */
    public function getEasyToFind()
    {
        return $this->getData(self::EASY_TO_FIND);
    }
    
    /**
     * Get Use Laptop
     *
     * @return int|null
     */
    public function getUseLaptop()
    {
        return $this->getData(self::USE_LAPTOP);
    }
    
    /**
     * Get Use Mobile
     *
     * @return int|null
     */
    public function getUseMobile()
    {
        return $this->getData(self::USE_MOBILE);
    }    
    
    /**
     * Get Use Mouse
     *
     * @return int|null
     */
    public function getUseMouse()
    {
        return $this->getData(self::USE_MOUSE);
    }    
    
    /**
     * Get Use Keyboard
     *
     * @return int|null
     */
    public function getUseKeyboard()
    {
        return $this->getData(self::USE_KEYBOARD);
    }    
    
    /**
     * Get Own Comments
     *
     * @return string|null
     */
    public function getOwnComments()
    {
        return $this->getData(self::OWN_COMMENTS);
    } 
    
    /**
     * Get Gender
     *
     * @return int|null
     */
    public function getGender()
    {
        return $this->getData(self::GENDER);
    }   
    
    /**
     * GetAge Group
     *
     * @return int|null
     */
    public function getAgeGroup()
    {
        return $this->getData(self::AGE_GROUP);
    }    
    
    /**
     * Return identities
     * @return string[]
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
    
    /**
     * Set ID
     *
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        return $this->setData(self::ANSWER_ID, $id);
    }
    
    /**
     * Set Created Timestamp
     *
     * @param string $createdAt
     * @return $this
     */
    public function setCreatedTimeStamp($createdAt)
    {
        return $this->setData(self::CREATED_TIMESTAMP, $createdAt);
    }            
    
    /**
     * Set User Friendly
     *
     * @param int $userFriendly
     * @return $this
     */
    public function setUserFriendly($userFriendly)
    {
        return $this->setData(self::USER_FRIENDLY, $userFriendly);
    }             

    /**
     * Set Easy to find
     *
     * @param int $easyToFind
     * @return $this
     */
    public function setEasyToFind($easyToFind)
    {
        return $this->setData(self::EASY_TO_FIND, $easyToFind);
    }            

    /**
     * Set Use Laptop
     *
     * @param int $useLaptop
     * @return $this
     */
    public function setUseLaptop($useLaptop)
    {
        return $this->setData(self::USE_LAPTOP, $useLaptop);
    }

    /**
     * Set Use Mobile
     *
     * @param int $useMobile
     * @return $this
     */
    public function setUseMobile($useMobile)
    {
        return $this->setData(self::USE_MOBILE, $useMobile);
    }             

    /**
     * Set Use Mouse
     *
     * @param int $useMouse
     * @return $this
     */
    public function setUseMouse($useMouse)
    {
        return $this->setData(self::USE_MOUSE, $useMouse);
    }             

    /**
     * Set Use Keyboard
     *
     * @param int $useKeyboard
     * @return $this
     */
    public function setUseKeyboard($useKeyboard)
    {
        return $this->setData(self::USE_KEYBOARD, $useKeyboard);
    }             

    /**
     * Set Own Comments
     *
     * @param string $ownComments
     * @return $this
     */
    public function setOwnComments($ownComments)
    {
        return $this->setData(self::OWN_COMMENTS, $ownComments);
    }             

    /**
     * Set Gender
     *
     * @param int $gender
     * @return $this
     */
    public function setGender($gender)
    {
        return $this->setData(self::GENDER, $gender);
    }             

    
    /**
     * Set Age Group
     *
     * @param int $ageGroup
     * @return $this
     */
    public function setAgeGroup($ageGroup)
    {
        return $this->setData(self::AGE_GROUP, $ageGroup);
    }             
    
    
}

